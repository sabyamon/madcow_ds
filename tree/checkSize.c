/*
 ============================================================================
 Name        : linkedlist.c
 Author      : sabya
 Version     : 1.0
 Copyright   : Your copyright notice
 Description : Binary Tree in C, Ansi-style
 ============================================================================
 */


#include <stdio.h>

/* A binary tree node has data, left child and right child */
struct node
{
    int data;
    struct node* left;
    struct node* right;
};




/* Computes the number of nodes in a tree. */
int size(struct node* node)
{
  if (node==NULL)
    return 0;
  else
    return(size(node->left) + 1 + size(node->right));
}



/* Helper function that allocates a new node with the given data and
   NULL left and right  pointers.*/

struct node* addNode(int data) {

	struct node *newNode ;
	newNode = (struct node*) malloc(sizeof (struct node)) ;

	newNode->data = data;
	newNode->left = NULL ;
	newNode->right = NULL ;

	return newNode ;

}


int main(){

	  struct node *root = addNode(1);
	  root->left        = addNode(2);
	  root->right       = addNode(3);
	  root->left->left  = addNode(4);
	  root->left->right = addNode(5);

	  printf("Size of the tree is %d", size(root));
	  getchar();
	  return 0;

}


