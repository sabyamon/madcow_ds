/*
 ============================================================================
 Name        : linkedlist.c
 Author      : sabya
 Version     : 1.0
 Copyright   : Your copyright notice
 Description : Check whether two linked lists are identical or not in C, Ansi-style
 ============================================================================
Time Complexity: O(n) for both iterative and recursive versions. n is the length of the smaller list among a and b.
 */


#include <stdio.h>


struct node{

	int data ;
	struct node *next;

};


int main(void) {

	struct node *a = NULL ;
	struct node *b = NULL ;

	insertFirst(&a , 1) ;
	insertFirst(&a , 2) ;
	insertFirst(&a , 3) ;

	insertFirst(&b , 1) ;
	insertFirst(&b , 2) ;
	insertFirst(&b , 3) ;

	if(areIdentical(a, b) == 1)
	    printf(" Linked Lists are identical ");
	  else
	    printf(" Linked Lists are not identical ");

	  getchar();
	  return 0;

}


/* returns 1 if linked lists a and b are identical, otherwise 0 */
int areIdentical(struct node *a, struct node *b)
	{
	  while(1)
	  {
		/* base case */
		if(a == NULL && b == NULL)
		{  return 1; }
		if(a == NULL && b != NULL)
		{  return 0; }
		if(a != NULL && b == NULL)
		{  return 0; }
		if(a->data != b->data)
		{  return 0; }

		/* If we reach here, then a and b are not NULL and their
		   data is same, so move to next nodes in both lists */
		a = a->next;
		b = b->next;
	  }
	}


void insertFirst(struct node **head , int data){

	struct node* new_node ;

	new_node = (struct node*) malloc(sizeof (struct node)) ;

	new_node->data = data ;
	new_node->next = *head ;

	//printf("address of **head is %d" , **head) ;

	*head = new_node ;

}


void printList(struct node *temp){

	while(temp != NULL){

		printf("%d " , temp->data) ;
		temp = temp->next ;

	}



}


