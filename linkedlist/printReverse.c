/*
 ============================================================================
 Name        : linkedlist.c
 Author      : sabya
 Version     : 1.0
 Copyright   : Your copyright notice
 Description : prints a linked list in reverse order in C, Ansi-style
 ============================================================================
 */


#include <stdio.h>


struct node{

	int data ;
	struct node *next;

};


int main(void) {

	struct node *head = NULL ;
	insertFirst(&head, 10);
	insertFirst(&head, 22);
	insertFirst(&head, 35);
	insertFirst(&head, 75);

	printf("Original List is \n");
	printList(head);

	printf("\nPrinting the list in reverse order\n");
	printReverse(head);


}

/* Function to reverse the linked list */
void printReverse(struct node* head)
{
  // Base case
  if(head == NULL)
    return;

  // print the list after head node
  printReverse(head->next);

  // After everything else is printed, print head
  printf("%d  ", head->data);
}


/*
 * This function reverses a linked list recursively .
 */
void reverseRecursive(struct node **head){

	struct node *first;
	struct node *rest;

	/* empty list */
	if(*head == NULL){
		return ;
	}

	first = *head ;
	rest = first->next;

	/* List has only one node */
	if(rest == NULL){
		return ;
	}
	/* reverse the rest list and put the first element at the end */
	reverseRecursive(&rest) ;

	first->next->next = first ;
	first->next = NULL ;

	/* fix the head pointer */
	*head = rest ;

}


void insertFirst(struct node **head , int data){

	struct node* new_node ;

	new_node = (struct node*) malloc(sizeof (struct node)) ;

	new_node->data = data ;
	new_node->next = *head ;

	//printf("address of **head is %d" , **head) ;

	*head = new_node ;

}


void printList(struct node *temp){

	while(temp != NULL){

		printf("%d " , temp->data) ;
		temp = temp->next ;

	}

}


